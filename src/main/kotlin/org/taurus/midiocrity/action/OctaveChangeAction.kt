package org.taurus.midiocrity.action

import java.awt.event.ActionEvent
import java.awt.event.KeyEvent
import javax.swing.AbstractAction
import javax.swing.JSpinner

class OctaveChangeAction(private val keyCode: Int) : AbstractAction() {

    override fun actionPerformed(e: ActionEvent) {
        val newOctave = keyCode + 1 - KeyEvent.VK_1
        if (newOctave !in 1..8) {
            throw IllegalArgumentException("Octave must be in range 1 through 8")
        }
        (e.source as JSpinner).value = newOctave
    }
}