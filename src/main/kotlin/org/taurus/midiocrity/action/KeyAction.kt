package org.taurus.midiocrity.action

import org.taurus.midiocrity.midi.Synthesizer
import java.awt.event.ActionEvent
import java.util.*
import java.util.concurrent.atomic.AtomicInteger
import javax.swing.AbstractAction
import javax.swing.JToggleButton

class KeyAction(private val type: ActionType, private val keyIndex: Int, private val key: JToggleButton, private val currentOctave: AtomicInteger, private val synthesizer: Synthesizer) : AbstractAction() {

    enum class ActionType {
        PRESSED, RELEASED
    }

    override fun actionPerformed(e: ActionEvent) {
        key.isSelected = type == ActionType.PRESSED
        val note = (2 + currentOctave.get()) * 12 + keyIndex
        println("${type.name} ${key.text}[$note]")
        if (type == ActionType.PRESSED) {
            synthesizer.noteOn(note)
        } else {
            synthesizer.noteOff(note)
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || other !is KeyAction) return false
        return keyIndex == other.keyIndex && type == other.type
    }

    override fun hashCode(): Int {
        return Objects.hash(type, keyIndex)
    }
}