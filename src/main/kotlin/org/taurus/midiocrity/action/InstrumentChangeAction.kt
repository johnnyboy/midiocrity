package org.taurus.midiocrity.action

import java.awt.event.ActionEvent
import javax.swing.AbstractAction
import javax.swing.JComboBox

class InstrumentChangeAction(private val direction: Direction, private val instrumentCount: Int) : AbstractAction() {
    enum class Direction {
        FORWARD, BACKWARD
    }

    override fun actionPerformed(e: ActionEvent) {
        val instruments = e.source as JComboBox<*>
        val index = if (direction == Direction.FORWARD) {
            Math.min(instrumentCount - 1, instruments.selectedIndex + 1)
        } else {
            Math.max(instruments.selectedIndex - 1, 0)
        }
        instruments.selectedIndex = index
    }
}