package org.taurus.midiocrity.midi

import javax.sound.midi.Instrument
import javax.sound.midi.MidiSystem

@Suppress("JoinDeclarationAndAssignment")
class Synthesizer {
    private val synth: javax.sound.midi.Synthesizer

    init {
        synth = MidiSystem.getSynthesizer()
        synth.open()
        val instrument = synth.availableInstruments[49]
        changeInstrument(instrument)
    }

    fun availableInstruments(): Array<Instrument> = synth.availableInstruments

    fun availableInstrumentsCount() = availableInstruments().size

    fun changeInstrument(instrument: Instrument) {
        synth.loadInstrument(instrument)
        channel().programChange(instrument.patch.program)
    }

    private fun channel() = synth.channels[0]

    fun instrument(): Instrument {
        return synth.loadedInstruments
                .find { i -> i.patch.program == channel().program } ?: throw IllegalStateException()
    }

    fun noteOn(note: Int) {
        channel().noteOn(note, 120)
    }

    fun noteOff(note: Int) {
        channel().noteOff(note)
    }

    fun close() = synth.close()

    fun mute() = channel().allSoundOff()
}