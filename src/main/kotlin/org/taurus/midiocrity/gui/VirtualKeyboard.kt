package org.taurus.midiocrity.gui

import org.taurus.midiocrity.action.InstrumentChangeAction
import org.taurus.midiocrity.action.KeyAction
import org.taurus.midiocrity.action.OctaveChangeAction
import org.taurus.midiocrity.midi.Synthesizer
import java.awt.BorderLayout
import java.awt.event.ActionEvent
import java.awt.event.KeyEvent
import java.util.concurrent.atomic.AtomicInteger
import javax.sound.midi.Instrument
import javax.swing.*
import javax.swing.event.ChangeEvent

class VirtualKeyboard(private val synthesizer: Synthesizer) {
    private val octave = AtomicInteger(3)

    init {
        setGUI()
    }

    private fun setGUI() {
        val frame = JFrame("Keyboard")
        frame.contentPane.add(BorderLayout.NORTH, keyPanel())
        frame.contentPane.add(BorderLayout.SOUTH, controlPanel())
        frame.addWindowListener(SynthesizerDisposableWindowAdapter(synthesizer))
        frame.defaultCloseOperation = WindowConstants.EXIT_ON_CLOSE
        frame.pack()
        frame.setLocationRelativeTo(null)
        frame.isVisible = true
    }

    private fun keyPanel(): JPanel {
        val keyPanel = JPanel()

        val notes = arrayOf("C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B")
        val keys = mutableListOf<JToggleButton>()

        for (i in notes.indices) {
            val key = JToggleButton(notes[i])
            key.isEnabled = false
            key.toolTipText = "Keyboard: F${i+1}"
            keyPanel.add(key)
            keys.add(key)
        }

        addKeyListeners(keyPanel, keys)
        return keyPanel
    }

    private fun addKeyListeners(keyPanel: JPanel, keys: List<JToggleButton>) {
        val inputMap = keyPanel.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
        val actionMap = keyPanel.actionMap
        for (keyCode in KeyEvent.VK_F1..KeyEvent.VK_F12) {
            val index = keyCode - KeyEvent.VK_F1
            val pressAction = KeyAction(KeyAction.ActionType.PRESSED, index, keys[index], octave, synthesizer)
            val releaseAction = KeyAction(KeyAction.ActionType.RELEASED, index, keys[index], octave, synthesizer)
            inputMap.put(KeyStroke.getKeyStroke(keyCode, 0), pressAction)
            actionMap.put(pressAction, pressAction)
            inputMap.put(KeyStroke.getKeyStroke(keyCode, 0, true), releaseAction)
            actionMap.put(releaseAction, releaseAction)
        }
    }

    private fun controlPanel(): JPanel {
        val instrumentChoiceLabel = JLabel("Instrument Choice:")
        instrumentChoiceLabel.toolTipText = "Keyboard '[' and ']'"
        val instruments = JComboBox(synthesizer.availableInstruments())
        instruments.selectedItem = synthesizer.instrument()
        instruments.setRenderer { _, value, _, _, _ -> JLabel(value.name) }
        instruments.isEditable = false
        instruments.addActionListener { handleInstrumentChange(it) }
        addInstrumentChangeKeyListeners(instruments)
        instrumentChoiceLabel.labelFor = instruments

        val octaves = JSpinner(SpinnerNumberModel(octave.get(), 1, 8, 1))
        octaves.addChangeListener { handleOctaveChange(it) }
        addOctaveChangeKeyListeners(octaves)
        val octaveChoiceLabel = JLabel("Octave:")
        octaveChoiceLabel.toolTipText = "Keyboard 1-8"
        octaveChoiceLabel.labelFor = octaves

        val controlPanel = JPanel()
        controlPanel.add(instrumentChoiceLabel)
        controlPanel.add(instruments)
        controlPanel.add(octaveChoiceLabel)
        controlPanel.add(octaves)

        return controlPanel
    }

    private fun addInstrumentChangeKeyListeners(instruments: JComboBox<Instrument>) {
        val inputMap = instruments.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
        val actionMap = instruments.actionMap
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_CLOSE_BRACKET.toChar()), KeyEvent.VK_CLOSE_BRACKET)
        actionMap.put(KeyEvent.VK_CLOSE_BRACKET, InstrumentChangeAction(InstrumentChangeAction.Direction.FORWARD, synthesizer.availableInstrumentsCount()))
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_OPEN_BRACKET.toChar()), KeyEvent.VK_OPEN_BRACKET)
        actionMap.put(KeyEvent.VK_OPEN_BRACKET, InstrumentChangeAction(InstrumentChangeAction.Direction.BACKWARD, synthesizer.availableInstrumentsCount()))
    }

    private fun addOctaveChangeKeyListeners(octaves: JSpinner) {
        val inputMap = octaves.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
        val actionMap = octaves.actionMap
        for (keyCode in KeyEvent.VK_1..KeyEvent.VK_8) {
            inputMap.put(KeyStroke.getKeyStroke(keyCode.toChar()), keyCode)
            actionMap.put(keyCode, OctaveChangeAction(keyCode))
        }
    }

    private fun handleInstrumentChange(e: ActionEvent) {
        val instrument = (e.source as JComboBox<*>).selectedItem as Instrument
        synthesizer.changeInstrument(instrument)
    }

    private fun handleOctaveChange(e: ChangeEvent) {
        octave.set((e.source as JSpinner).value as Int)
    }
}