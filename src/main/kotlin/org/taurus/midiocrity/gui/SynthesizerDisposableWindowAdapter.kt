package org.taurus.midiocrity.gui

import org.taurus.midiocrity.midi.Synthesizer
import java.awt.event.WindowAdapter
import java.awt.event.WindowEvent

class SynthesizerDisposableWindowAdapter(private val synthesizer: Synthesizer) : WindowAdapter() {

    override fun windowClosing(e: WindowEvent?) = synthesizer.close()

    override fun windowDeactivated(e: WindowEvent?) = synthesizer.mute()
}