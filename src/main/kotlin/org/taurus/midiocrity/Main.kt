package org.taurus.midiocrity

import org.taurus.midiocrity.gui.VirtualKeyboard
import org.taurus.midiocrity.midi.Synthesizer
import javax.swing.SwingUtilities

fun main(args: Array<String>) {
    SwingUtilities.invokeLater {
        VirtualKeyboard(Synthesizer())
    }
}