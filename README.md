### midiocrity

A simple Swing-based synthesizer written in Kotlin

#### controls

`F1-F12` - notes  
`1-8` - octave selection  
`[` `]` - instrument selection

#### build
./gradlew clean build

#### run
java -jar build/libs/midiocrity.jar